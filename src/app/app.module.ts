import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './site/header/header.component';
import { SliderComponent } from './site/slider/slider.component';
import { InfoboardComponent } from './site/infoboard/infoboard.component';
import { FooterComponent } from './site/footer/footer.component';
import { LoginComponent } from './authenticate/login/login.component';
import { RegisterComponent } from './authenticate/register/register.component';
import { ConfirmpasswordComponent } from './authenticate/confirmpassword/confirmpassword.component';
import { SiteComponent } from './site/site.component';
import { DashboardComponent } from './users/dashboard/dashboard.component';
import { ForgetpasswordComponent } from './authenticate/forgetpassword/forgetpassword.component';
import { CreateComponent } from './users/dashboard/create/create.component';
import { ViewComponent } from './users/dashboard/view/view.component';
import { EditComponent } from './users/dashboard/edit/edit.component';
import { SettingsComponent } from './users/settings/settings.component';
import { SearchComponent } from './users/dashboard/search/search.component';
import { RemoveComponent } from './users/dashboard/remove/remove.component';
import { ChangepasswordComponent } from './authenticate/changepassword/changepassword.component';
import { UploaddocumentsComponent } from './site/uploaddocuments/uploaddocuments.component';
import { ScandocumentComponent } from './scandocument/scandocument.component';


const appRoutes: Routes = [
  {path: '', component: SiteComponent},
  {path: 'Home', component: SiteComponent},
  // {path: 'About-Us', component: AboutComponent},
  // {path: 'Recipes', component: RecipeComponent},
  // {path: 'Gallery', component: RecipeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'account/password/forget', component: ForgetpasswordComponent},
  {path: 'account/password/confirm', component: ConfirmpasswordComponent},
  {path: 'account/password/change', component: ChangepasswordComponent},
  {path: 'Upload-Documents', component: UploaddocumentsComponent},
  {path: 'scan/document', component: ScandocumentComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    InfoboardComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    ConfirmpasswordComponent,
    SiteComponent,
    DashboardComponent,
    ForgetpasswordComponent,
    CreateComponent,
    ViewComponent,
    EditComponent,
    SettingsComponent,
    SearchComponent,
    RemoveComponent,
    ChangepasswordComponent,
    UploaddocumentsComponent,
    ScandocumentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
