import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-uploaddocuments',
  templateUrl: './uploaddocuments.component.html',
  styles: []
})
export class UploaddocumentsComponent implements OnInit {

 selectedFile: File = null;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile.name);

  }

  onUpload() {
    // upload code goes here
    const uploadData = new FormData();
    uploadData.append('photo', this.selectedFile, this.selectedFile.name);
    console.log(uploadData);
    this.http.post('http://localhost:3000/api/upload', uploadData, {
      reportProgress: true,
      observe: 'events'
    })
      .subscribe(response => {
        console.log(response); // handle event here
      });

      // this.http.get(apiResourse).subscribe(res => {
      //
      // })
  }
}
